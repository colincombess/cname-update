variable "cnames" {
  type        = list(string)
  description = "List of comma-separated values in the form: <cname>,<record>."
}

variable "arecords" {
  type        = list(string)
  description = "List of comma-separated values in the form: <record>,<ip>."
}

variable "txts" {
  type        = list(string)
  description = "List of comma-separated values in the form: <record>,<value>."
}

variable "mxs" {
  type        = list(string)
  description = "List of comma-separated values in the form: <record>,<value>."
}

