cnames = [
  "business.parkmobile.io,6069772.group22.sites.hubspot.net",
  "alerts.parkmobile.io,u10326341.wl092.sendgrid.net",
  "abmail.alerts.parkmobile.io,u10326341.wl092.sendgrid.net",
  "s1._domainkey.alerts.parkmobile.io,s1.domainkey.u10326341.wl092.sendgrid.net",
  "s2._domainkey.alerts.parkmobile.io,s2.domainkey.u10326341.wl092.sendgrid.net",
  "mail.parkmobile.io,u10326341.wl092.sendgrid.net",
  "abmail.mail.parkmobile.io,u10326341.wl092.sendgrid.net",
  "s1._domainkey.mail.parkmobile.io,s1.domainkey.u10326341.wl092.sendgrid.net",
  "s2._domainkey.mail.parkmobile.io,s2.domainkey.u10326341.wl092.sendgrid.net",
  "ablink.mail.parkmobile.io,thirdparty.bnc.lt",
  "10326341.mail.parkmobile.io,sendgrid.net",
  "hs1._domainkey.b.parkmobile.io,b-parkmobile-io.hs01a.dkim.hubspotemail.net",
  "hs2._domainkey.b.parkmobile.io,b-parkmobile-io.hs01b.dkim.hubspotemail.net",
  "email.parkmobile.io,6069772.group22.sites.hubspot.net",
  "email.gh-mail.parkmobile.io,mailgun.org"
]
arecords = [
  "o2135.abmail.alerts.parkmobile.io,149.72.177.76",
  "o1948.alerts.parkmobile.io,168.245.4.148",
  "o2134.abmail.mail.parkmobile.io,149.72.154.254",
  "o1947.mail.parkmobile.io,167.89.46.178"
]

txts = [
  "smtpapi._domainkey.b.parkmobile.io,k=rsa; t=s; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDPtW5iwpXVPiH5FzJ7Nrl8USzuY9zqqzjE0D1r04xDN6qwziDnmgcFNNfMewVKN2D1O+2J9N14hRprzByFwfQW76yojh54Xu3uSbQ3JP0A7k8o8GutRF8zbFUA8n0ZH2y0cIEjMliXY4W4LwPA7m4q0ObmvSjhd63O9d8z1XkUBwIDAQAB",
  "gh-mail.parkmobile.io,v=spf1 include:mg-spf.greenhouse.io ~all",
  "krs._domainkey.gh-mail.parkmobile.io,k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC+vnIrdjiGUL+Mr1HkzYwLNe1nVPJzirMKwW1EyzKu8x3KN8SKg242cfc1xSCE1H9mtdh1tlv2XdbMMHvsxlbV4aA7aoEV4rRnKXjRcKwNeLO/nlDp3FLKeMxRttsGgp94f7jDA41mSlD63qo7ZOIx8ViIoq/sDmIKqJF3XmDlMQIDAQAB",

]
# txts = ["_oktaverification.parkmobile.io","b60b919b6a544393a5de68a6ebed290d"]

mxs = [
  "gh-mail.parkmobile.io,10 mxa.mailgun.org",
]
