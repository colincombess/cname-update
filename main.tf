data "aws_route53_zone" "selected" {
  name = "parkmobile.io."
}

resource "aws_route53_record" "record" {
  count   = length(compact(var.cnames))
  zone_id = data.aws_route53_zone.selected.zone_id
  name    = element(split(",", element(compact(var.cnames), count.index)), 0)
  type    = "CNAME"
  ttl     = "300"
  records = [element(split(",", element(compact(var.cnames), count.index)), 1)]
}

resource "aws_route53_record" "ip" {
  count   = length(compact(var.arecords))
  zone_id = data.aws_route53_zone.selected.zone_id
  name    = element(split(",", element(compact(var.arecords), count.index)), 0)
  type    = "A"
  ttl     = "300"
  records = [element(split(",", element(compact(var.arecords), count.index)), 1)]
}

resource "aws_route53_record" "txt" {
  count   = length(compact(var.txts))
  zone_id = data.aws_route53_zone.selected.zone_id
  name    = element(split(",", element(compact(var.txts), count.index)), 0)
  type    = "TXT"
  ttl     = "300"
  records = [element(split(",", element(compact(var.txts), count.index)), 1)]
}

resource "aws_route53_record" "mxs" {
  count   = length(compact(var.mxs))
  zone_id = data.aws_route53_zone.selected.zone_id
  name    = element(split(",", element(compact(var.mxs), count.index)), 0)
  type    = "MX"
  ttl     = "3600"
  records = [element(split(",", element(compact(var.mxs), count.index)), 1)]
}
